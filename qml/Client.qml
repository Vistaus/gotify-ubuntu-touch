import QtQuick 2.7

Item {
    function getUrl(path, appToken) {
        var url = settings.url;
        if (settings.url.indexOf('http://') == -1 && settings.url.indexOf('https://') == -1) {
            url = 'http://' + settings.url;
        }

        if (url[url.length - 1] != '/') {
            url += '/';
        }

        url += path + '?token=';
        url += appToken ? settings.appToken : settings.clientToken;
        return url;
    }

    function sendRequest(method, path, body, appToken, callback) {
        var xhr = new XMLHttpRequest();

        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4) {
                var data = null;
                if (xhr.responseText) {
                    try {
                        data = JSON.parse(xhr.responseText);
                    }
                    catch (e) {
                        console.log('json error', e);
                        return callback('Error in server response', null);
                    }
                }

                if (xhr.status == 200) {
                    return callback(null, data);
                }
                else {
                    console.log('error', xhr.status);
                    var error = 'Unknown error';
                    if (data && data.errorDescription) {
                        error = data.errorDescription;
                    }

                    return callback(error, null);
                }
            }
        };

        xhr.open(method, getUrl(path, appToken), true);
        xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');

        if (body) {
            xhr.send(JSON.stringify(body));
        }
        else {
            xhr.send();
        }
    }

    function sendMessage(title, message, callback) {
        var body = {
            title: title,
            message: message,
            extras: {
                'ubuntu-touch::ignore': true,
            },
        };
        sendRequest('POST', 'message', body, true, callback);
    }

    function getAllMessages(callback) {
        sendRequest('GET', 'message', null, false, callback);
    }

    function deleteMessage(id, callback) {
        sendRequest('DELETE', 'message/' + id, null, false, callback);
    }

    function getVersion(callback) {
        sendRequest('GET', 'version', null, true, callback);
    }
}
